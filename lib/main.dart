import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/services.dart' as prefix0;
import './DUMMY_DART.dart';
import './Models/MealsModel.dart';
import './Screens/SettingScreen.dart';
import './Screens/TapSreen.dart';
import './Screens/MealsScreen.dart';
import './Screens/MealDescriptionScreen.dart';

void main() {
  runApp(MyApp()) ;
   SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]) ;
}

class MyApp extends StatefulWidget {
  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String mealId;
  bool isFavorite;
  List<Meal> currentMeals = DUMMY_MEALS;

  Map<String, Object> Filters = {
    'gulten': false,
    'lactose': false,
    'vegan': false,
    'vegetrian': false
  };

  void SaveFilters(Map<String, Object> Setting) {
    setState(() {
      Filters = Setting;
      currentMeals = DUMMY_MEALS.where((meal) {
        if (Filters['gulten'] && !meal.isGlutenFree) {
          return false;
        }
        if (Filters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        if (Filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (Filters['vegetrian'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  List<Meal> favouriteList = [];

  void AddOrRemoveMealtoFavouriteScreen(String id) {
    int SelectedIndex = favouriteList.indexWhere((meal) {
      return meal.id == id;
    });
    if (SelectedIndex >= 0) {
      setState(() {
        favouriteList.removeAt(SelectedIndex);
      });
    }
    if (SelectedIndex < 0) {
      setState(() {
        favouriteList.add(DUMMY_MEALS.firstWhere((meal) => meal.id == id));
      });
    }
  }

  bool isFavourite(String id) {
    return favouriteList.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Delimeals',
      theme: ThemeData(
          primaryColor: Colors.pink,
          accentColor: Colors.white,
          fontFamily: 'Raleway'),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (ctx) => TapsScreen(favouriteList),
        MealsScreen.RouteName: (ctx) => MealsScreen(currentMeals),
        MealDescriptionScreen.RouteName: (ctx) =>
            MealDescriptionScreen(
                AddOrRemoveMealtoFavouriteScreen, isFavourite),
        SettingScreen.RouteName: (ctx) =>
            SettingScreen(
              save: SaveFilters,
              filters: Filters,
            ),
      },
    );
  }
}
// tomorow i will add meals to
// favourite Screen by access to its id in meals description
