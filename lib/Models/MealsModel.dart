enum Affordability { Affordable, Luxurious, Pricey }
enum Complexity { Simple, Hard, Challenging }

class Meal {
  final String id;

  final String title;

  final List<String> categories;
  final List<String> ingredients;

  final List<String> steps;

  final int duration;

  final Affordability affordability;

  final Complexity complexity;

  final String imageUrl;

  final bool isGlutenFree;

  final bool isVegan;

  final bool isVegetarian;

  final bool isLactoseFree;

  const Meal(
      {this.complexity,
      this.affordability,
      this.id,
      this.title,
      this.imageUrl,
      this.duration,
      this.categories,
      this.isLactoseFree,
      this.isVegetarian,
      this.isGlutenFree,
      this.isVegan,
      this.steps,
      this.ingredients});
}
