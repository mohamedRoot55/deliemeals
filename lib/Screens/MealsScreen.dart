import 'package:flutter/material.dart';
import 'package:re_delimeals/Widget/MealsItem.dart';
import '../Models/MealsModel.dart';
import '../DUMMY_DART.dart';

class MealsScreen extends StatefulWidget {
  static const String RouteName = 'MealsScreen';
 final List<Meal> CurrentData ;
  MealsScreen(this.CurrentData);
  @override
  State createState() => _MealsScreenState();
}

class _MealsScreenState extends State<MealsScreen> {
  List<Meal> FilterdDataByCategoryId;


  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context).settings.arguments as List<Map<String, Object>>;
    FilterdDataByCategoryId = widget.CurrentData.where((meal) {
      return meal.categories.contains(arguments[0]['id']);
    }).toList();
    return Scaffold(
      appBar: AppBar(
        title: Text(arguments[0]['title']),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: FilterdDataByCategoryId.length,
          itemBuilder: (ctx, index) {
            return MealsItem(
              id: FilterdDataByCategoryId[index].id,
              complexity: FilterdDataByCategoryId[index].complexity,
              affordability: FilterdDataByCategoryId[index].affordability,
              duration: FilterdDataByCategoryId[index].duration,
              title: FilterdDataByCategoryId[index].title,
              imageUrl: FilterdDataByCategoryId[index].imageUrl,
            );
          },
        ),
      ),
    );
  }
}
