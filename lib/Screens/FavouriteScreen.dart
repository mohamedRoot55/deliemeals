import 'package:flutter/material.dart';
import '../Models/MealsModel.dart';
import '../Widget/MealsItem.dart';

class FavouriteScreen extends StatefulWidget {
  static const RouteName = '/FavouriteSreen';

  final List<Meal> favoriteList;

  FavouriteScreen(this.favoriteList);

  @override
  State createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: widget.favoriteList.isEmpty
            ? Container(child: Center(child: Text('add some favourite .. ' , style: TextStyle(color: Colors.pink,fontSize: 23),)),)
            : ListView.builder(
                itemCount: widget.favoriteList.length,
                itemBuilder: (ctx, index) {
                  return MealsItem(
                    id: widget.favoriteList[index].id,
                    complexity: widget.favoriteList[index].complexity,
                    affordability: widget.favoriteList[index].affordability,
                    duration: widget.favoriteList[index].duration,
                    title: widget.favoriteList[index].title,
                    imageUrl: widget.favoriteList[index].imageUrl,
                  );
                },
              ),
      ),
    );
  }
}
