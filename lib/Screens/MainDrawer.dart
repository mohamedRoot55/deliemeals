import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Screens/SettingScreen.dart';


class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 150,
              child: Center(
                child: Text(
                  'Setting your app',
                  style: TextStyle(
                      color: Colors.pink,
                      fontSize: 23,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),

            InkWell(
              onTap: (){Navigator.of(context).pushReplacementNamed('/');},
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: Colors.grey), borderRadius: BorderRadius.circular(15)),
                margin: EdgeInsets.all(20),
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      'Meals',
                      style: TextStyle(color: Colors.pink, fontSize: 20),
                    ),
                    Icon(
                      Icons.restaurant,
                      color: Colors.pink,
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: (){Navigator.of(context).pushReplacementNamed(SettingScreen.RouteName);},
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: Colors.grey), borderRadius: BorderRadius.circular(15)),
                margin: EdgeInsets.all(20),
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      'Setting',
                      style: TextStyle(color: Colors.pink, fontSize: 20),
                    ),
                    Icon(
                      Icons.settings,
                      color: Colors.pink,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
