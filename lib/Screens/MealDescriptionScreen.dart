import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Models/MealsModel.dart';
import '../DUMMY_DART.dart';

class MealDescriptionScreen extends StatefulWidget {
  static const String RouteName = '/MealDescriptionScreen';
  final Function AddOrRemoveMealtoFavouriteScreen;

  final Function isFavourite;

  MealDescriptionScreen(
      this.AddOrRemoveMealtoFavouriteScreen, this.isFavourite);

  @override
  State createState() => _MealDescriptionScreenState();
}

class _MealDescriptionScreenState extends State<MealDescriptionScreen> {
  Meal meal;

//  @override
//  void initState() {
//    // TODO: implement initState
//    final MealId = ModalRoute.of(context).settings.arguments as String;
//     meal = DUMMY_MEALS.firstWhere((meal) => meal.id == MealId);
//    super.initState();
//  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    final MealId = ModalRoute.of(context).settings.arguments as String;
    meal = DUMMY_MEALS.firstWhere((meal) => meal.id == MealId);
    super.didChangeDependencies();
  }

  var isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: 800,
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Container(
                child: Image.network(
                  meal.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Card(
                child: Text('ingresiants'),
                elevation: 3,
                margin: EdgeInsets.only(top: 3, bottom: 3),
              ),
              Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey)),
                height: 180,
                width: 200,
                child: ListView.builder(
                  itemCount: meal.ingredients.length,
                  itemBuilder: (ctx, index) {
                    return Card(
                      margin: EdgeInsets.only(top: 3, bottom: 3),
                      color: Colors.yellowAccent,
                      child: Text(meal.ingredients[index]),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Card(
                child: Text('steps'),
                elevation: 3,
                margin: EdgeInsets.only(top: 3, bottom: 3),
              ),
              Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey)),
                height: 200,
                width: 300,
                child: ListView.builder(
                  itemCount: meal.steps.length,
                  itemBuilder: (ctx, index) {
                    return Card(
                      color: Colors.yellowAccent,
                      child: ListTile(
                        leading: CircleAvatar(
                          child: Text(index.toString()),
                        ),
                        title: Text(meal.steps[index]),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          widget.AddOrRemoveMealtoFavouriteScreen(meal.id);
        },
        child: widget.isFavourite(meal.id)
            ? Icon(Icons.star)
            : Icon(Icons.star_border),
      ),
    );
  }
}
