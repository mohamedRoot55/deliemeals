import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Models/MealsModel.dart' ;
import './MainDrawer.dart';
import './CategoriesScreen.dart';
import './FavouriteScreen.dart';

class TapsScreen extends StatefulWidget {
  final List<Meal> FavouriteList ;
  TapsScreen(this.FavouriteList);
  @override
  State createState() => _TapsScreenState();
}

class _TapsScreenState extends State<TapsScreen> {
  int _selectedIndex = 0;

  void onTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, Object>> data = [
      {'page': CategoriesScreen(), 'title': 'categories'},
      {'title': 'favourite', 'page': FavouriteScreen(widget.FavouriteList)}
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(data[_selectedIndex]['title']),
      ),
      drawer: MainDrawer(),
      body: data[_selectedIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: true,
        selectedItemColor: Colors.amber,
        unselectedItemColor: Colors.white,
        backgroundColor: Colors.pink,
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.shifting,
        onTap: onTap,
        items: [
          BottomNavigationBarItem(
              backgroundColor: Colors.pink,
              icon: Icon(Icons.category),
              title: Text(
                'categories',
                style: TextStyle(color: Colors.white),
              )),
          BottomNavigationBarItem(
              backgroundColor: Colors.pink,
              icon: Icon(Icons.favorite),
              title: Text(
                'favourites',
                style: TextStyle(color: Colors.white),
              )),
        ],
      ),
    );
  }
}
