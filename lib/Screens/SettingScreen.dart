import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Screens/MainDrawer.dart';

class SettingScreen extends StatefulWidget {
  static const String RouteName = '/SettingScreen';
  Map<String, Object> filters;
  final Function save;

  SettingScreen({this.save, this.filters});

  @override
  State createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool isLactose = false;

  bool isGluten = false;

  bool isVegan = false;

  bool isVegetarian = false;

  @override
  void initState() {
    // TODO: implement initState

    isLactose = widget.filters['lactose'];
    isGluten = widget.filters['gulten'];
    isVegan = widget.filters['vegan'];
    isVegetarian = widget.filters['vegetrian'];

    super.initState();
  }

  Widget makeSwitchcard(
      String title, String subtitle, isBool, Function updateValue) {
    return Card(
      margin: EdgeInsets.all(5),
      child: SwitchListTile(
        value: isBool,
        title: Text(
          title,
          style: TextStyle(color: Colors.pink),
          softWrap: true,
        ),onChanged: updateValue,
        subtitle: Text(subtitle),
        activeColor: Colors.amber,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Setting'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              widget.save({
                'gulten': isGluten,
                'lactose': isLactose,
                'vegan': isVegan,
                'vegetrian': isVegetarian
              });
            },
          ),
        ],
      ),
      drawer: MainDrawer(),
      body: Container(
        padding: const EdgeInsets.all(24),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 30),
              child: Center(
                child: Text(
                  'Setting your app as you want',
                  style: TextStyle(color: Colors.pink, fontSize: 20),
                  softWrap: true,
                ),
              ),
            ),
            makeSwitchcard(
                'free Lactose', 'this meal is free from Lactose', isLactose,
                (val) {
              setState(() {
                isLactose = val;
              });
            }),
            makeSwitchcard(
                'free Gluten', 'this meal is free from Gluten', isGluten,
                (val) {
              setState(() {
                isGluten = val;
              });
            }),
            makeSwitchcard('isVegan', 'this meal is free from lactose', isVegan,
                (val) {
              setState(() {
                isVegan = val;
              });
            }),
            makeSwitchcard('isVegetreian', 'this meal is free from vegtables',
                isVegetarian, (val) {
              setState(() {
                isVegetarian = val;
              });
            }),
          ],
        ),
      ),
    );
  }
}
