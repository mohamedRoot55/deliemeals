import 'package:flutter/material.dart';
import '../Models/CategoriesModel.dart';
import '../DUMMY_DART.dart';
import '../Widget/CategoriesItem.dart';

class CategoriesScreen extends StatelessWidget {
  static const String RouteName = '/CategoriesScreen' ;
  List<Category> data = DUMMY_CATEGORIES;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(10),
      child: GridView(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        children: data.map((category) {
          return CategoryItem(
              id: category.id, title: category.title, color: category.color);
        }).toList(),
      ),
    );
  }
}
