import 'package:flutter/material.dart';
import '../Screens/MealsScreen.dart';

class CategoryItem extends StatelessWidget {
  final String id;

  final String title;

  final Color color;

  CategoryItem({this.id, this.title, this.color});

  void NavigateToMealsScreenwhiteSomeData(BuildContext ctx) {
    List<Map<String, Object>> arguments = [
      {'id': id, 'title': title, 'color': color}
    ];
    Navigator.of(ctx).pushNamed(MealsScreen.RouteName, arguments: arguments);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => NavigateToMealsScreenwhiteSomeData(context),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),
            gradient: LinearGradient(
                colors: [color.withOpacity(0.7), color],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft)),
        padding: const EdgeInsets.all(0.6),
        child: Center(
          child: Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
