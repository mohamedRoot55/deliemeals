import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:re_delimeals/Models/MealsModel.dart';
import '../Screens/MealDescriptionScreen.dart';

class MealsItem extends StatelessWidget {
  final String title;

  final String imageUrl;

  final int duration;

  final String id;

  final Affordability affordability;

  final Complexity complexity;

  MealsItem(
      {this.complexity,
      this.affordability,
      this.title,
      this.id,
      this.duration,
      this.imageUrl});

  String get checkAffordability {
    switch (affordability) {
      case Affordability.Luxurious:
        // TODO: Handle this case.
        return 'Luxurious';
        break;
      case Affordability.Pricey:
        // TODO: Handle this case.
        return 'Pricey';
        break;
      case Affordability.Affordable:
        // TODO: Handle this case.
        return 'Affordable';
        break;
    }
    return 'notFound';
  }

  String get checkComplexity {
    switch (complexity) {
      case Complexity.Simple:
        return 'Simple';
        break;
      case Complexity.Hard:
        return 'Hard';

        break;
      case Complexity.Challenging:
        return 'Challenging';

        break;
    }
    return 'not found';
  }

  void NavigateToMealsDescriptionScreen(BuildContext context) {
    Navigator.of(context)
        .pushNamed(MealDescriptionScreen.RouteName, arguments: id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => NavigateToMealsDescriptionScreen(context),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        elevation: 6,
        margin: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  child: Image.network(
                    imageUrl,
                    fit: BoxFit.cover,
                    height: 250,
                    width: double.infinity,
                  ),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                      width: 220,
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                      decoration: BoxDecoration(
                        color: Colors.black45,
                      ),
                      child: Text(title,
                          style: TextStyle(color: Colors.white, fontSize: 24),
                          softWrap: true)),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.access_time),
                      Text('${duration}')
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.attach_money),
                      Text(checkAffordability)
                    ],
                  ),
                  Row(
                    children: <Widget>[Icon(Icons.work), Text(checkComplexity)],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
